# vagrant-oracle-soa12110-dev-desktop

Mit dieser Vagrant-Konfiguration wird ein Mint 18  "Sarah" System mit folgenden Softwarekomponenten
konfiguriert:

* Mint Desktop
* Firefox Browser
* Java JDK 8 + Browser-Plugin * Zertifikat für esentri Artifactory
* JDeveloper 12.2.1.1. und SOA
* git
* Shell mit Maven-Konfiguration

Mit dem Benutzer vagrant/vagrant kann die VM genutzt werden.

## Voraussetzungen

1. Virtual Box muss auf dem Hostsystem installiert und konfiguriert sein.
   (http://www.oracle.com/technetwork/server-storage/virtualbox/downloads/index.html)

2. vagrant muss auf dem Hostsystem installiert und konfiguriert sein. 
   (https://www.vagrantup.com/downloads.html)


## Vorbereitung

1. Download SOA Quickstart Installer 12.2.1.1 vom OTN und Datei nach ./files entpacken
	http://download.oracle.com/otn/nt/middleware/12c/122110/fmw_12.2.1.1.0_soaqs_Disk1_1of2.zip
	http://download.oracle.com/otn/nt/middleware/12c/122110/fmw_12.2.1.1.0_soaqs_Disk1_2of2.zip

2. Projekt von git beziehen.


## Verwendung


### Virtuelle Maschine starten

1. Commandline auf dem Hostsystem öffnen und in den Rootordner des ausgecheckten Repositories wechseln.

2. Startkommando ausführen

   `
   #> vagrant up
   `


### Virtuelle Maschine stoppen

1. Commandline auf dem Hostsystem öffnen und in den Rootordner des ausgecheckten Repositories wechseln.

2. Stopkommando ausführen

   `
   #> vagrant halt
   `

### Sonstiges

1. Ein deutsches Keyboard muss ggf. manuell nach dem Start der VM über die Einstellungen konfiguriert werden.

2. Die Datumseinstellungen können durch Ausführen des Kommandos nachträglich korrigiert werden:

   `
   #> sudo dpkg-reconfigure tzdata
   `

3. Anpassung der Datei $HOME/.m2/settings.xml mit den persönlichen Credentials für den Zugriff auf Artifactory.



