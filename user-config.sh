#!/bin/bash

echo "Run user-config.sh..."
echo "Noch ein user output mehr."

echo "Prepare desktop..."
mkdir -p "$HOME/Desktop"

export FMW_HOME=/u01/app/oracle/product/12.2.1.1/soaquickstart

echo "Installing Oracle JDeveloper 12.2.1.1 with SOA..."
if ! [ -f "$FMW_HOME/jdeveloper/jdev/bin/jdev" ] 
  then
     if [ -f "/vagrant/files/fmw_12.2.1.1.0_soa_quickstart.jar" ] 
       then          
          cp /vagrant/files/jdev-install.rsp /tmp
          echo "ORACLE_HOME="$FMW_HOME >> /tmp/jdev-install.rsp
          java -jar /vagrant/files/fmw_12.2.1.1.0_soa_quickstart.jar -silent -responseFile /tmp/jdev-install.rsp -ignoreSysPrereqs > /dev/null
          echo "AddVMOption -Duser.timezone=CET" >> $FMW_HOME/jdeveloper/jdev/bin/jdev.conf

          cp -f /vagrant/files/jdeveloper12211.desktop "$HOME/Desktop/"
          chmod +x $HOME/Desktop/jdeveloper12211.desktop
          echo "Exec="$FMW_HOME"/jdeveloper/jdev/bin/jdev" >> $HOME/Desktop/jdeveloper12211.desktop
       else
         echo "JDeveloper 12.2.1.1 must be downloaded and available unter ./files."
     fi
fi

echo "Prepare Maven..."
source ~/.profile
if [ -z "$M2_HOME" ]
   then
      echo "Adds Maven Settings to profile..."
      export M2_HOME=/usr/share/maven 
      echo "export M2_HOME=/usr/share/maven" >> ~/.profile
      echo "export M2=$M2_HOME/bin" >> ~/.profile
	  echo "export PATH=$M2:$PATH" >> ~/.profile
fi

if [ -z "$TZ" ]
   then
      echo "Adds Timezone Settings to profile..."
      export TZ='CET'
      echo "export TZ='CET'" >> ~/.profile
fi

echo "Preparing Keyboard"
echo "setxkbmap de" >> ~/.profile

