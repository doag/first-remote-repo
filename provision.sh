#!/bin/bash

sudo rm /var/lib/dpkg/lock

if ! [ $(id -u) = 0 ]; then
   echo "This script must be run as root."
   exit 1
fi

# suppress some warnings
export DEBIAN_FRONTEND=noninteractive

# if there is no home directory assume user is not existing
#if ! [ -d "/home/vagrant" ] 
# then
#	  echo "ceate vagrant user"
#	  sudo useradd -p $(openssl passwd -1 vagrant) vagrant -d '/home/vagrant' -g sudo
#	  echo "create vagrant group"
#	  groupadd vagrant
#	  echo "adding user to group"
#	  sudo adduser vagrant vagrant
#	  sudo adduser vagrant sudo
#	  sudo mkdir /home/vagrant/
#	  sudo chmod 777 /home/vagrant
#fi

echo "Installing ubuntu-desktop..."
sudo apt-get update > /dev/null
sudo apt-get install -y --no-install-recommends firefox xterm > /dev/null
sudo apt-get install -y language-pack-de > /dev/null
echo "switching to german keyboard binding"
setxkbmap de
echo "ubuntu-dekstop installed."
 
echo "Installing wget..."
sudo apt-get install -y wget > /dev/null

echo "Install libstdc..."
sudo apt-get install -y libstdc++6 > /dev/null
sudo apt-get install -y libstdc++5 > /dev/null

echo "Installing firefox..."
if ! which firefox > /dev/null
  then
     sudo apt-get install -y firefox > /dev/null
fi
echo "Configure Firefox..."
if [ -f "/etc/firefox/pref/syspref.js" ] 
   then
      echo "user_pref(\"browser.startup.homepage\", \"file:///vagrant/files/index.html\");" >> /etc/firefox/pref/syspref.js
fi
if [ -f "/etc/firefox/syspref.js" ] 
   then
      echo "user_pref(\"browser.startup.homepage\", \"file:///vagrant/files/index.html\");" >> /etc/firefox/syspref.js
fi

echo "Installing git ..."
if ! which git > /dev/null
  then
     sudo apt-get install git > /dev/null
fi
  
echo "Installing Java JDK 8..."
sudo apt-get install -y python-software-properties > /dev/null
sudo add-apt-repository ppa:webupd8team/java -y
sudo apt-get update
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
sudo apt-get install -y oracle-java8-installer > /dev/null

sudo apt-get install oracle-java8-set-default > /dev/null

echo "Installing Jenkins..."
wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install -y jenkins > /dev/null

echo "Installing Maven ..."
if ! which mvn > /dev/null
  then
     sudo apt-get install -y maven > /dev/null
fi
  
echo "Installing certificate for esentri Artifactory..."
if ! [ -f "/tmp/certfile.txt" ] 
   then
     openssl s_client -connect repo.esentri.com:443 2>&1 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/certfile.txt
     sudo /usr/lib/jvm/java-8-oracle/bin/keytool -import -alias "repo.esentri.com" -file /tmp/certfile.txt -keystore /usr/lib/jvm/java-8-oracle/jre/lib/security/cacerts -storepass changeit -noprompt
fi
 
echo "Installing Artifactory..."
if ! [ -f "/etc/init.d/artifactory" ] 
  then
     sudo unzip /vagrant/files/jfrog-artifactory-oss-4.14.0.zip -d /opt > /dev/null
     sudo /opt/artifactory-oss-4.14.0/bin/installService.sh > /dev/null
     echo "JAVA_HOME=/usr/lib/jvm/java-8-oracle" >> /etc/opt/jfrog/artifactory/default
     sudo service artifactory start
fi

echo "Prepare Oracle installations..."
if ! [ -f "/etc/oraInst.loc" ] 
  then
     sudo cp /vagrant/files/oraInst.loc /etc/
     sudo chmod o+r /etc/oraInst.loc
fi

if ! [ -f "/usr/share/pixmaps/jdev_icon.gif" ] 
  then
     echo "Copy jdev_icon.gif to pixmaps..."
     sudo cp -f /vagrant/files/jdev_icon.gif /usr/share/pixmaps/
     sudo chmod o+r /usr/share/pixmaps/jdev_icon.gif
fi

# Oracle XE installation
if ! [ -f "/etc/init.d/oracle-xe" ] 
  then
     if [ -f "/vagrant/files/oracle-xe-11.2.0-1.0.x86_64.rpm" ]
       then 
          echo "Install additional OS packages for Oracle XE..."
          sudo apt-get install -y alien libaio1 unixodbc > /dev/null

          echo "set up kernel parameters - most of the dbs need shared memory..."
          sudo cp -f /vagrant/files/60-oracle.conf /etc/sysctl.d/60-oracle.conf
          sudo service procps start

          echo "Prepare Ubuntu OS for Oracle XE..."
          sudo cp -f /vagrant/files/chkconfig /sbin/chkconfig
          sudo chmod 755 /sbin/chkconfig
          sudo ln -s /usr/bin/awk /bin/awk
          mkdir -p /var/lock/subsys
          touch /var/lock/subsys/listener
          
          echo "Converts rpm file..."
          sudo alien --scripts -d /vagrant/files/oracle-xe-11.2.0-1.0.x86_64.rpm
         
          echo "Install Oracle XE 11.2..."
          sudo dpkg -i oracle-xe_11.2.0-2_amd64.deb | sudo tee /tmp/XEsilentinstall.log
          sudo rm -rf /dev/shm
          sudo mkdir /dev/shm
          # The size parameter should contain the Memory in MB configured for the VM.
          sudo mount -t tmpfs shmfs -o size=10144m /dev/shm
          sudo cp -f /vagrant/files/S01shm_load /etc/rc2.d/S01shm_load
          sudo chmod 755 /etc/rc2.d/S01shm_load
          sudo /etc/init.d/oracle-xe configure responseFile=/vagrant/files/xe-install.rsp | sudo tee -a /tmp/XEsilentinstall.log
          sudo rm oracle-xe_11.2.0-2_amd64.deb
          
       else
          echo "Oracle XE 11.2 must be downloaded and available unter ./files."
     fi
fi

echo "Setup privileges on Oracle-Home..."
sudo usermod -a -G dba vagrant
sudo chown -R oracle /u01
sudo chgrp -R dba /u01
sudo chmod -R g+rwx /u01

echo "Installing user-specific things..."
su -c "source /vagrant/user-config.sh" vagrant

echo "Again setup privileges on Oracle-Home..."
sudo chown -R oracle /u01
sudo chgrp -R dba /u01
sudo chmod -R g+rwx /u01
